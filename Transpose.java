public class Transpose {
    public static void main(String[] args){
        //create matrix (starting at number 1
        
        int size = Integer.parseInt(args[0]);
        
        int[][] myMatrix = new int[size][size];
        
        for(int i=0; i < size; i++){
            for(int j=0; j < size; j++){
                myMatrix[i][j] = i*size + j + 1;
            }
        }
        
        //display
        for(int i=0; i < size; i++){
            for(int j=0; j < size; j++){
                System.out.printf("%4d", myMatrix[i][j]);
            }
            System.out.println();
        }
        System.out.println("Transpose:");
        
        //transpose (do not use second matrix, do this in place instead)
        for(int i=0; i < size; i++){
            for(int j=i+1; j < size; j++){
                int tempValue = myMatrix[i][j];
                myMatrix[i][j] = myMatrix[j][i];
                myMatrix[j][i] = tempValue;
            }
        }
        
        //display
        for(int i=0; i < size; i++){
            for(int j=0; j < size; j++){
                System.out.printf("%4d", myMatrix[i][j]);
            }
            System.out.println();
        }
    }
}